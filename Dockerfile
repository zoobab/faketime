FROM debian:bullseye
RUN apt -y update
RUN apt install -y faketime
RUN cp -v /usr/lib/x86_64-linux-gnu/faketime/libfaketime.so.1 /lib/libfaketime.so
ENV LD_PRELOAD=/lib/libfaketime.so
ENV FAKETIME="-1m"
ENV DONT_FAKE_MONOTONIC=1
RUN date
